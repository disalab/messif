# MESSIF: Metric Similarity Search Implementation Framework #
The similarity search has become a fundamental computational task in many applications. One of the mathematical models of the similarity – the metric space – has drawn attention of many researchers resulting in several sophisticated metric-indexing techniques. An important part of a research in this area is typically a prototype implementation and subsequent experimental evaluation of the proposed data structure. Individual structures are often based on very similar underlying principles or even exploit some existing structures on lower levels. Therefore, the implementation calls for a uniform development platform that would support a straightforward reusability of code. Such a framework would also simplify the experimental evaluation, make the comparison more fair and thus the results would be of greater value.

The Metric Similarity Search Implementation Framework (MESSIF) pursue the following objectives:

* to provide basic support for the indexing based on metric space – let developers focus on the higher-level design;
* to create a uniﬁed and semi-automated mechanism for measuring and collecting statistics;
* to deﬁne and use uniform interfaces to support modularity and thus allow reusing of the code;
* to provide infrastructure for distributed processing with focus on peer-to-peer paradigm – communication support, deployment, monitoring, testing, etc.;
* to support complex similarity search in multi-metric spaces.

If you use this library for academic purposes, please cite the ​[following paper](http://www.springerlink.com/content/x486534524622785/):

Michal Batko, David Novák and Pavel Zezula. MESSIF: Metric Similarity Search Implementation Framework. In Digital Libraries: Research and Development, Lecture Notes in Computer Science, vol. 4877. Berlin, Heidelberg : Springer-Verlag, 2007, 10 pages, ISBN 978-3-540-77087-9.

## Contact Person ##
Michal Batko, [homepage](http://www.muni.cz/people/2907?lang=en)

David Novak, [homepage](http://disa.fi.muni.cz/david-novak/)

## Licence
MESSIF library is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. MESSIF library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.