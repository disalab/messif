/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.utility;

import java.util.Arrays;
import junit.framework.TestCase;
import messif.objects.text.WordsProvider;
import messif.objects.text.impl.WordsJaccardDistanceFunction;

/**
 *
 * @author Michal Batko <batko@fi.muni.cz>
 */
public class SortingIteratorTest extends TestCase {

    public SortingIteratorTest(String testName) {
        super(testName);
    }

    public WordsProvider createWordsProvider(final String... words) {
        return new WordsProvider() {
            @Override
            public String[] getWords() {
                return words;
            }
        };
    }

    public SortingIterator<String> createIterator(final String... words) {
        return new SortingIterator<String>().addArray(words);
    }

    private void intersectionBothWays(String[] expected, String[] words1, String[] words2) {
        IteratorIntersection<String> intersection = new IteratorIntersection<>(createIterator(words1), createIterator(words2), null);
        assertEquals(Arrays.asList(expected), intersection.intersection());
        intersection = new IteratorIntersection<>(createIterator(words2), createIterator(words1), null);
        assertEquals(Arrays.asList(expected), intersection.intersection());
    }

    public void testIntersection() {
        intersectionBothWays(new String[] {"ahoj", "ahoj", "cau", "cau", "nazdar"}, new String[] { "ahoj", "ahoj", "cau", "cau", "nazdar" }, new String[] {"ahoj", "cau", "nazdar"});
        intersectionBothWays(new String[] {"cau", "cau", "nazdar", "nazdar"}, new String[] { "ahoj", "cau", "nazdar", "nazdar" }, new String[] {"cau", "cau", "nazdar"});
        intersectionBothWays(new String[] {"ahoj", "zdar"}, new String[] { "ahoj", "cau", "nazdar", "zdar" }, new String[] {"a", "ahoj", "zdar"});
    }

    public void testWordsJaccardDistanceFunction() {
        WordsJaccardDistanceFunction d = new WordsJaccardDistanceFunction();
        assertEquals(0f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar"), createWordsProvider("ahoj", "cau", "nazdar")));
        assertEquals(0f, d.getDistance(createWordsProvider("ahoj", "ahoj", "cau", "cau", "nazdar"), createWordsProvider("ahoj", "ahoj", "cau", "cau", "nazdar")));
        assertEquals(1f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar"), createWordsProvider()));
        assertEquals(1f, d.getDistance(createWordsProvider(), createWordsProvider("ahoj", "cau", "nazdar")));
        assertEquals(1f, d.getDistance(createWordsProvider("a", "b", "c"), createWordsProvider("ahoj", "cau", "nazdar")));
        assertEquals(1f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar"), createWordsProvider("a", "b", "c")));
        assertEquals(1.0f - 2.0f/7.0f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar", "zdar"), createWordsProvider("a", "ahoj", "zdar")), 0.001);
        assertEquals(1.0f - 3.0f/8.0f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar", "zdar"), createWordsProvider("a", "ahoj", "ahoj", "zdar")), 0.001);
    }
}
