/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.buckets.storage.impl;

import java.io.File;
import java.io.IOException;
import java.nio.channels.AsynchronousFileChannel;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import messif.buckets.BucketStorageException;
import messif.buckets.storage.LongAddress;
import messif.objects.nio.AsynchronousFileChannelInputStream;
import messif.objects.nio.AsynchronousFileChannelOutputStream;
import messif.objects.nio.BinarySerializator;
import messif.utility.Convert;

/**
 * Disk based storage inheriting almost everything from {@link DiskStorage}.
 * The only difference is that this storage assumes storing objects of the same sizes (or several
 *  specific fixed sizes) and thus, when storing new object to storage with "gaps" (deleted objects)
 *  the storage tries to find a gap with exactly the same size as the object to be inserted. The
 *  process of finding the gap may take a while since the storage must iterate over all stored 
 *  objects.
 *
 * @param <T> the class of objects stored in this storage
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DiskStorageFillGaps<T> extends DiskStorage<T> {
    /** Class serial id for Java serialization. */
    private static final long serialVersionUID = 2L;

    /** List of gaps with their sizes. This is kept only in memory and reconstructed after deserialization. */
    private transient SortedMap<Long, Integer> deletedFragmentList;


    //****************** Constructors ******************//

    /**
     * Creates a new {@link DiskStorageFillGaps} instance.
     *
     * @param storedObjectsClass the class of objects that the new storage will work with
     * @param file the file in which to create the bucket
     * @param readonly if <tt>true</tt>, the storage will be opened in read-only mode (e.g. the store method will throw an exception)
     * @param bufferSize the size of the buffer used for reading/writing
     * @param bufferDirect the bucket is either direct (<tt>true</tt>) or array-backed (<tt>false</tt>)
     * @param asyncThreads the maximal number of threads to use (for asynchronous reading)
     * @param startPosition the position in the file where this storage starts
     * @param maximalLength the maximal length of the file
     * @param serializator the object responsible for storing (and restoring) binary objects
     * @throws IOException if there was an error opening the bucket file
     */
    public DiskStorageFillGaps(Class<? extends T> storedObjectsClass, File file, boolean readonly, int bufferSize, boolean bufferDirect, int asyncThreads, long startPosition, long maximalLength, BinarySerializator serializator) throws IOException {
        super(storedObjectsClass, file, readonly, bufferSize, bufferDirect, asyncThreads, startPosition, maximalLength, serializator);
    }

    /**
     * Creates a new DiskStreamStorage instance.
     * All parameters are copied from the provided <code>copyAttributesDiskStorage</code>
     * except for the file name. The bucket is always opened in read-write mode.
     *
     * @param copyAttributesDiskStorage the disk storage from which to copy parameters
     * @param file the file in which to create the bucket
     * @throws IOException if there was an error opening the bucket file
     */
    public DiskStorageFillGaps(final DiskStorage<? extends T> copyAttributesDiskStorage, File file) throws IOException {
        super(copyAttributesDiskStorage, file);
        log.log(Level.INFO, "Created new storage of type {0}", DiskStorageFillGaps.class.getCanonicalName());
    }


    //****************** Factory method ******************//

    /**
     * Creates a new disk storage which fills the gaps. The additional parameters are specified in the parameters map with
     * the following recognized key names:
     * <ul>
     *   <li><em>file</em> - the path to the particular disk storage (either as File or as String)</li>
     *   <li><em>dir</em> - the path to a directory (either as File or as String) where a temporary file name is
     *       created in the format of "disk_storage_XXXX.ds"</li>
     *   <li><em>cacheClasses</em> - comma-separated list of classes that will be cached for fast serialization</li>
     *   <li><em>bufferSize</em> - the size of the buffers used for I/O operations</li>
     *   <li><em>directBuffer</em> - flag controlling whether to use faster direct buffers for I/O operations</li>
     *   <li><em>readOnly</em> - if <tt>true</tt>, the storage file must be a valid storage file and the storage will support only read operations</li>
     *   <li><em>startPosition</em> - the position (in bytes) of the first block of the data within the <em>file</em></li>
     *   <li><em>maximalLength</em> - the maximal length (in bytes) of the data written to <em>file</em> by this storage</li>
     *   <li><em>oneStorage</em> - if <tt>true</tt>, the storage is created only once
     *              and this created instance is used in subsequent calls</li>
     *   <li><em>serializator</em> - instance of the serializator that is used (overrides any cacheClasses settings)</li>
     * </ul>
     *
     * @param <T> the class of objects that the new storage will work with
     * @param storedObjectsClass the class of objects that the new storage will work with
     * @param parameters list of named parameters (see above)
     * @return a new disk storage instance
     * @throws IOException if something goes wrong when working with the filesystem
     * @throws InstantiationException if the parameters specified are invalid (non existent directory, null values, etc.)
     */
    public static <T> DiskStorageFillGaps<T> create(Class<T> storedObjectsClass, Map<String, Object> parameters) throws IOException, InstantiationException {
        DiskStorage<T> stdDiskStorage = DiskStorage.<T>create(storedObjectsClass, parameters);
        
        // this can happen in case the DiskStorage.create method only took an already created storage from the parameters ("oneStorage")
        if (stdDiskStorage instanceof DiskStorageFillGaps) {
            return (DiskStorageFillGaps) stdDiskStorage;
        }

        boolean oneStorage = Convert.getParameterValue(parameters, "oneStorage", Boolean.class, false);
        if (oneStorage && (null != castToDiskStorage(storedObjectsClass, Convert.getParameterValue(parameters, "storage", DiskStorageFillGaps.class, null)))) {
            throw new IllegalArgumentException("trying to create DiskStorageFillGaps with param 'oneStorage' but the passed 'storage' is not instance of 'DiskStorageFillGaps'");
        }
        
        DiskStorageFillGaps<T> retVal = new DiskStorageFillGaps<>(stdDiskStorage, stdDiskStorage.getFile());

        // Save the created storage for subsequent calls
        if (oneStorage && parameters != null)
            parameters.put("storage", retVal);

        return retVal;
    }

    // ******************    Overrides    ****************************** //

    @Override
    protected synchronized void readHeader(AsynchronousFileChannel fileChannel, long position) throws IOException {
        super.readHeader(fileChannel, position);
        if (deletedFragmentList == null) { // Recostruct was not called, so the map is stored in the storage at occupation position
            AsynchronousFileChannelInputStream stream = takeInputStream(getFileOccupation(false));
            try {
                if (getSerializator().readInt(stream) != 0) // Read ending null that separate the fragmented list from the objects
                    throw new IOException("Incorrect data detected, expected zero object size!");
                deletedFragmentList = getSerializator().readObject(stream, TreeMap.class);
                if (deletedFragmentList == null)
                    throw new IOException("Incorrect data detected, expected some deleted fragment list but null was in the storage!");
            } finally {
                returnInputStream(stream);
            }
        }
    }

    @Override
    protected synchronized void writeHeader(AsynchronousFileChannel fileChannel, long position, int flags) throws IOException {
        // Write the deleted fragment list at the occupation position if the file is closing and was modified
        if ((flags & FLAG_CLOSED) == FLAG_CLOSED && isModified()) {
            AsynchronousFileChannelOutputStream outputStream = this.getOutputStream();
            long currentOutputPos = outputStream.getPosition();
            try {
                getSerializator().write(outputStream, 0); // Write ending null to separate the fragmented list from the objects
                getSerializator().write(outputStream, deletedFragmentList);
            } finally {
                outputStream.setPosition(currentOutputPos);
            }
        }
        super.writeHeader(fileChannel, position, flags);
        if (deletedFragmentList == null) // This is necessary when a new file was created
            deletedFragmentList = new TreeMap<>();
    }

    @Override
    protected synchronized void reconstructHeader(AsynchronousFileChannel fileChannel, long position) throws IOException {
        this.deletedFragmentList = new TreeMap<>();
        super.reconstructHeader(fileChannel, position);
    }
    
    @Override
    protected void addDeletedFragment(long position, int size) {
        super.addDeletedFragment(position, size); 
        deletedFragmentList.put(position, size);
    }

    @Override
    protected void removeDeletedFragment(long position) {
        super.removeDeletedFragment(position);
        deletedFragmentList.remove(position);
    }

    /**
     * This method either finds a position in the middle of the file where is space to store given
     * object, or it returns <tt>null</tt>. If in the middle, the free spot
     * must contain an integer whose absolute value is the size of the passed object.
     *
     * @param object object to find a spot for
     * @return position in the file that should accommodate given object
     */
    protected synchronized Long getPositionToStore(T object) {
        if (deletedFragmentList == null || deletedFragmentList.isEmpty())
            return null;

        // find the size of the object
        int binarySize = getSerializator().getBinarySize(object);
        for (Map.Entry<Long, Integer> fragment : deletedFragmentList.entrySet()) {
            if (fragment.getValue() == binarySize) {
                return fragment.getKey();
            }
        }

        return null;
    }

    @Override
    public synchronized LongAddress<T> store(T object) throws BucketStorageException {
        // get a position to store the object to (default is the end)
        Long positionToStore = getPositionToStore(object);
        if (positionToStore == null)
            return super.store(object);

        // Object is stored in the middle of the file
        rewrite(object, positionToStore);
        removeDeletedFragment(positionToStore);
        return new LongAddress<>(this, positionToStore);
    }

    // FIXME: this operation WILL break indexes built on this storage
    @Override
    protected synchronized void compactData() throws IOException {
        log.warning("Compacting the disk storage, which breaks all indexes above this storage (address pointing into the storage)");
        super.compactData();
        if (getDeletedFragments() == 0) {
            deletedFragmentList.clear();
        }
    }
}
