/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import messif.objects.util.AbstractObjectIterator;
import messif.operations.AbstractOperation;
import messif.operations.AnswerType;
import messif.operations.OperationErrorCode;
import messif.operations.QueryOperation;
import messif.operations.RankingSingleQueryOperation;

/**
 * A batch of several ranking query operations encapsulated as a single operation.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Honza Brázdil, janinko.g@gmail.com
 * @param <T> Type of the ranking query operation.
 */
@AbstractOperation.OperationName("k-nearest neighbors query")
public class BatchRankingQueryOperation<T extends RankingSingleQueryOperation> extends QueryOperation {

    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    private final List<T> operations ;

    //****************** Constructors ******************//

    /**
     * Creates a list of {@link RankingSingleQueryOperation} for all specified query operation.
     * @param operations Collection of ranking operations.
     */
    @AbstractOperation.OperationConstructor({"Query object", "Number of nearest objects"})
    public BatchRankingQueryOperation(Collection<T> operations) {
        this(operations, AnswerType.NODATA_OBJECTS);
    }

    /**
     * Creates a list of {@link RankingSingleQueryOperation} for all specified query operation.
     * @param operations Collection of ranking operations.
     * @param answerType the type of objects this operation stores in its answer
     */
    @AbstractOperation.OperationConstructor({"Query object", "Number of nearest objects", "Answer type"})
    public BatchRankingQueryOperation(Collection<T> operations, AnswerType answerType) {
        super(answerType);
        this.operations = Collections.unmodifiableList(new ArrayList<>(operations));
        for(T operation : operations){
            operation.setAnswerType(answerType);
        }
    }

    //****************** Attribute access ******************//

    public int getNOperations() {
        return operations.size();
    }
    
    public T getOperation(int index) {
        return operations.get(index);
    }

    public List<T> getKnnOperations() {
        return operations;
    }
    

    /**
     * Returns argument that was passed while constructing instance.
     * If the argument is not stored within operation, <tt>null</tt> is returned.
     * @param index index of an argument passed to constructor
     * @return argument that was passed while constructing instance
     * @throws IndexOutOfBoundsException if index parameter is out of range
     */
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        default:
            throw new IndexOutOfBoundsException("kNNQueryOperation has only two arguments");
        }
    }

    /**
     * Returns number of arguments that were passed while constructing this instance.
     * @return number of arguments that were passed while constructing this instance
     */
    @Override
    public int getArgumentCount() {
        return 0;
    }

    @Override
    public boolean wasSuccessful() {
        return getErrorCode() == OperationErrorCode.RESPONSE_RETURNED;
    }

    @Override
    public void endOperation() {
        endOperation(OperationErrorCode.RESPONSE_RETURNED);
    }

    @Override
    public int evaluate(AbstractObjectIterator objects) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class getAnswerClass() {
        if (operations.isEmpty()) {
            return null;
        }
        return (operations.get(0).getAnswerClass());
    }

    @Override
    public int getAnswerCount() {
        return 0;
    }

    @Override
    public Iterator getAnswer() {
        return Collections.emptyIterator();
    }

    @Override
    public Iterator getAnswer(int skip, int count) {
        return Collections.emptyIterator();        
    }

    @Override
    public Iterator getAnswerObjects() {
        return Collections.emptyIterator();
    }

    @Override
    public void resetAnswer() {
    }

    @Override
    public int getSubAnswerCount() {
        return operations.size();
    }

    @Override
    public Iterator getSubAnswer(int index) throws IndexOutOfBoundsException {
        return operations.get(index).getAnswer();
    }

    @Override
    public Iterator getSubAnswer(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean dataEqualsImpl(QueryOperation operation) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int dataHashCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
