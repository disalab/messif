/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.operations.query;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.OperationErrorCode;
import messif.utility.ErrorCode;

/**
 * Dumps objects (IDs or data content) to separate files based on the depth of the object in a structure, where the path within the structure
 * is dumped before it. The output files are plain-text files named "level-xyz.txt" and their lines are of one of the formats:
 * <pre>path &lt;TAB&gt; locator</pre>
 * <pre>path &lt;TAB&gt; data</pre>
 *
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 */
@AbstractOperation.OperationName("Dump objects to files")
public class DumpObjectsToFilesOperation extends AbstractOperation {

    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** Flag whether object's data will be dumped or just object's ID (locator) */
    private final boolean dumpObjectDescriptors;

    /** Name of the directory to output data to */
    private final String outputDir;
    
    /** Flag whether the object's are dumped based on pivot balls (range queries) rather than leaf node contents */
    private final boolean queryByPivotBalls;
    /** Radius multiple for pivot covering radii -- to make balls smaller/larger on request */
    private final float pivotBallRadiusMultiple;

    
    // ***************   Output attribute     ********************* //
    
    /** List of output streams (one per "level")  */
    private final List<OutputStream> outputFiles;
    

    /****************** Constructors ******************/

    /**
     * Creates a new instance of operation.
     * @param dumpObjectDescriptors if true, descriptor (data) instead of object ID is printed
     * @param outputDir path to directory where all files (per "level") are created
     */
    @AbstractOperation.OperationConstructor({"objects as descriptors", "output directory", "query by pivot balls", "pivot ball radius multiple"})
    public DumpObjectsToFilesOperation(boolean dumpObjectDescriptors, String outputDir, 
                    boolean queryByPivotBalls, float pivotBallRadiusMultiple) throws IllegalArgumentException {
        super();
        this.dumpObjectDescriptors = dumpObjectDescriptors;
        this.outputDir = outputDir;
        this.queryByPivotBalls = queryByPivotBalls;
        this.pivotBallRadiusMultiple = pivotBallRadiusMultiple;
        File dir = new File(outputDir);
        if (!dir.exists())
            dir.mkdirs();
        this.outputFiles = new ArrayList<>();
    }

    /**
     * Returns argument that was passed while constructing instance.
     * If the argument is not stored within operation, <tt>null</tt> is returned.
     * @param index index of an argument passed to constructor
     * @return argument that was passed while constructing instance
     * @throws IndexOutOfBoundsException if index parameter is out of range
     */
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        case 0:
            return dumpObjectDescriptors;
        case 1:
            return outputDir;
        case 2:
            return queryByPivotBalls;
        case 3:
            return pivotBallRadiusMultiple;
        default:
            throw new IndexOutOfBoundsException(this.getClass().getSimpleName() + " has only two arguments");
        }
    }

    /**
     * Returns number of arguments that were passed while constructing this instance.
     * @return number of arguments that were passed while constructing this instance
     */
    @Override
    public int getArgumentCount() {
        return 4;
    }
    
    @Override
    public boolean wasSuccessful() {
        return getErrorCode() == OperationErrorCode.RESPONSE_RETURNED;
    }

    @Override
    public void endOperation() {
        endOperation(OperationErrorCode.RESPONSE_RETURNED);
    }

    @Override
    public void endOperation(ErrorCode errValue) throws IllegalArgumentException {
        super.endOperation(errValue);
        // Close all files
        for (OutputStream out : outputFiles) {
            try { out.close(); } catch (IOException ignore) { }
        }
    }
        
    public void addToAnswer(int level, List<String> objectPath, LocalAbstractObject o) {
        try {
            while (outputFiles.size() < level) {
                outputFiles.add(openFile(outputFiles.size()));
            }
            
            // range queries already containes all object on 1st level (so use the last one only!)
            int localLevel = (queryByPivotBalls) ? level - 1 : 0;       
            for (; localLevel < level; localLevel++) {
                OutputStream out = outputFiles.get(localLevel);
                out.write(getPath(objectPath, localLevel + 1).getBytes());
                out.write('\t');
                if (dumpObjectDescriptors) {
                    o.write(out, false);
                } else {
                    out.write(o.getLocatorURI().getBytes());
                    out.write('\n');
                }
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
    
    public void addToAnswer(int level, List<String> objectPath, Iterator<? extends AbstractObject> objects) {
        while (objects.hasNext()) {
            AbstractObject o = objects.next();
            addToAnswer(level, objectPath, (LocalAbstractObject)o);
        }
    }
    
    private OutputStream openFile(int level) throws FileNotFoundException {
        File f = new File(outputDir, String.format(Locale.ENGLISH, "level-%d.txt", level+1));
        return new BufferedOutputStream(new FileOutputStream(f));
    }

    private String getPath(List<String> objectPath, int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            if (sb.length() > 0)
                sb.append('.');
            sb.append(objectPath.get(i));
        }
        return sb.toString();
    }

}
