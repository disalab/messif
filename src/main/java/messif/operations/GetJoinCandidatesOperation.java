/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import static messif.operations.OperationErrorCode.RESPONSE_RETURNED;
import messif.operations.query.JoinQueryOperation;

/**
 * This operation is supposed to return sets of object IDs (locators) that are candidates for 
 *  {@link JoinQueryOperation}. These sets are to be checked one with each other.
 *  After the processing of this operation is finished, it is expected that method 
 * {@link AbstractOperation#endOperation(messif.utility.ErrorCode)} 
 *  is called; at the same time, the answer queue should stop blocking
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class GetJoinCandidatesOperation extends AbstractOperation {

    /** Class id for serialization. */
    private static final long serialVersionUID = 622301L;
    
    /** Name of the parameter to restrict the number of candidate sets to be retrieved by this operation. */
    public static final String MAX_SETS_PARAM = "MAX_SETS_PARAM";
    
    /** 
     * Maximal number of candidate sets to be stored in the result of this operation. If full, the 
     * result queue will be blocked until other process reads something from the queue. 
     */
    public static final int QUEUE_SIZE = 1000;
    
    /** 
     * Output of the operation stored in a blocking queue. 
     */
    protected final BlockingQueue<List<String>> candidateLocatorSets;

    /**
     * Creates a new operation.
     * @param joinOperation the similarity self join operation to find candidate set for
     */
    @AbstractOperation.OperationConstructor({"similarity join operation"})
    public GetJoinCandidatesOperation(JoinQueryOperation joinOperation) {
        this.candidateLocatorSets = new ArrayBlockingQueue<>(QUEUE_SIZE);
        copyParameters(joinOperation, false);
    } 
    
    
    // ************************      Getters and setters     ***************************** //

    public BlockingQueue<List<String>> getCandidateLocatorSets() {
        return candidateLocatorSets;
    }
    
    // ************************     Data manipulation    ***************************** //
    
    /**
     * Adds given set of candidate IDs (locators) to the answer.
     * @param candidateSet candidate IDs
     * @return true if all objects from the iterator were added to the answer, false otherwise
     */
    public boolean addList(List<String> candidateSet) {
        try {
            candidateLocatorSets.put(candidateSet);
            return true;
        } catch (InterruptedException ex) {
            return false;
        }
    }
    
    // ************************      Overrides     ******************************** //
    
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        throw new IndexOutOfBoundsException(GetJoinCandidatesOperation.class.getName() + "does not have any argument");
    }

    @Override
    public int getArgumentCount() {
        return 0;
    }
    
    @Override
    public void updateFrom(AbstractOperation operation) throws ClassCastException {
        if (! (operation instanceof GetJoinCandidatesOperation))
            throw new IllegalArgumentException(getClass().getSimpleName() + " cannot be updated from " + operation.getClass().getSimpleName());        
        if (this == operation) {
            return;
        }
        GetJoinCandidatesOperation castOp = (GetJoinCandidatesOperation) operation;
        try {
        while (! castOp.candidateLocatorSets.isEmpty()) {
                addList(castOp.candidateLocatorSets.take());
        }
        } catch (InterruptedException ignore) { }
        super.updateFrom(operation);                
    }
    
    @Override
    public boolean wasSuccessful() {
        return (getErrorCode() == RESPONSE_RETURNED);
    }

    @Override
    public void endOperation() {
        endOperation(RESPONSE_RETURNED);
    }
    
}
