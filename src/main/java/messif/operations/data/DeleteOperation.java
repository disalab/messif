/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import messif.buckets.BucketErrorCode;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;

/**
 * Operation for deleting object(s).
 * The operation keeps a list of {@link messif.objects.AbstractObject abstract object}.
 * All the objects that are {@link messif.objects.LocalAbstractObject#dataEquals data equal} to one 
 * of these specified object are deleted from an index structure. Additionally, the operation
 * specifies max number of objects to be deleted for each of the specified object
 * and flag, if the locator is to be checked.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("Delete")
public class DeleteOperation extends DataManipulationOperation {
    
    /** Class serial id for Java serialization. */
    private static final long serialVersionUID = 1L;
    
    /** Parameter that stores info about not deleted object IDs (in case some were missed). */
    public static final String NOT_DELETED_IDS_PARAM = "NOT DELETED";

    //****************** Attributes ******************//

    /** Objects to match the data against. */
    private List<LocalAbstractObject> objectsToDelete;

    /** Maximal number of actually deleted objects for each specified object, zero means unlimited. */
    private final int deleteLimit;

    /** Flag whether to check that the deleted object's locator is equal to {@link #deletedObjects}'s locators. */
    private final boolean checkLocator;

    /** List of all actually deleted objects. */
    private final List<LocalAbstractObject> objectsActuallyDeleted;


    //****************** Constructors ******************//

    /**
     * Creates a new instance of DeleteOperation.
     * @param deletedObject the object to match the data against
     * @param deleteLimit the maximal number of deleted objects (zero means unlimited)
     * @param checkLocator flag whether to check that the deleted object's locator is equal to {@link #objectsToDelete}'s locator
     */
    @AbstractOperation.OperationConstructor({"Object to delete", "Limit for # of deletions", "Check locator when deleting"})
    public DeleteOperation(LocalAbstractObject deletedObject, int deleteLimit, boolean checkLocator) {
        this(Collections.singletonList(deletedObject), deleteLimit, checkLocator);
    }

    /**
     * Creates a new instance of DeleteOperation.
     * @param deletedObject the object to match the data against
     * @param deleteLimit the maximal number of deleted objects (zero means unlimited)
     * @param checkLocator flag whether to check that the deleted object's locator is equal to {@link #objectsToDelete}'s locator
     */
    public DeleteOperation(List<LocalAbstractObject> deletedObject, int deleteLimit, boolean checkLocator) {
        this.objectsToDelete = deletedObject;
        this.deleteLimit = deleteLimit;
        this.checkLocator = checkLocator;
        this.objectsActuallyDeleted = new ArrayList<>();
    }
    
    /**
     * Creates a new instance of DeleteOperation.
     * @param deletedObject the object to match the data against
     * @param deleteLimit the maximal number of deleted objects (zero means unlimited)
     */
    @AbstractOperation.OperationConstructor({"Object to delete", "Limit for # of deletions"})
    public DeleteOperation(LocalAbstractObject deletedObject, int deleteLimit) {
        this(deletedObject, deleteLimit, false);
    }

    /**
     * Creates a new instance of DeleteOperation.
     * @param deletedObject the object to match the data against
     */
    @AbstractOperation.OperationConstructor({"Object to delete"})
    public DeleteOperation(LocalAbstractObject deletedObject) {
        this(deletedObject, 0);
    }

    
    //******************   Clonning    ********************* //
    
    @Override
    public DeleteOperation clone() throws CloneNotSupportedException {
        DeleteOperation operation = (DeleteOperation)super.clone();
        if (operation.objectsToDelete != null) {
            ArrayList<LocalAbstractObject> arrayList = new ArrayList<>(objectsToDelete.size());
            for (LocalAbstractObject obj : operation.objectsToDelete) {
                arrayList.add(obj.clone());
            }
            operation.objectsToDelete = arrayList;
        }
        return operation;
    }
        

    //****************** Attribute access ******************//

    /**
     * Returns the objects against which to match the deleted objects.
     * @return the objects against which to match the deleted objects
     */
    public List<LocalAbstractObject> getObjectsToDelete() {
        return Collections.unmodifiableList(objectsToDelete);
    }

    /**
     * Returns the maximal number of deleted objects.
     * Zero means unlimited.
     * @return the maximal number of deleted objects.
     */
    public int getDeleteLimit() {
        return deleteLimit;
    }

    /**
     * Returns the flag whether to check that the deleted object's locator
     * is equal to {@link #objectsToDelete}'s locator.
     * @return the flag whether to check the locator when deleting
     */
    public boolean isCheckingLocator() {
        return checkLocator;
    }

    /**
     * Returns the list of all actually deleted objects.
     * @return the list of all actually deleted objects
     */
    public List<LocalAbstractObject> getObjects() {
        return Collections.unmodifiableList(objectsActuallyDeleted);
    }

    /**
     * Mark the specified object as deleted by this operation.
     * @param deletedObject the object that was deleted
     */
    public void addDeletedObject(LocalAbstractObject deletedObject) {
        objectsActuallyDeleted.add(deletedObject);
    }

    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
            case 0:
                return getObjectsToDelete();
            case 1:
                return getDeleteLimit();
            case 2:
                return isCheckingLocator();
            default:
                throw new IndexOutOfBoundsException("Delete operation has only two arguments");
        }
    }

    @Override
    public int getArgumentCount() {
        return 3;
    }

    /**
     * This method sets the parameter showing object IDs that were not deleted by this operation.
     * @param notDeleted list of objects that were not deleted
     */
    public void setNotDeleted(Collection<LocalAbstractObject> notDeleted) {
        StringBuilder buffer = new StringBuilder();
        for (LocalAbstractObject obj : notDeleted) {
            buffer.append(obj.getLocatorURI()).append(", ");
        }
        buffer.delete(buffer.length() - 2, buffer.length());
        setParameter(NOT_DELETED_IDS_PARAM, buffer.toString());
    }

    //****************** Implementation of abstract methods ******************//

    @Override
    public boolean wasSuccessful() {
        return isErrorCode(BucketErrorCode.OBJECT_DELETED);
    }

    @Override
    public void endOperation() {
        endOperation(BucketErrorCode.OBJECT_DELETED);
    }

    @Override
    public void updateFrom(AbstractOperation operation) {
        DeleteOperation castOperation = (DeleteOperation)operation;
        for (LocalAbstractObject object : castOperation.objectsActuallyDeleted)
            this.objectsActuallyDeleted.add(object);

        super.updateFrom(operation);
    }    

    @Override
    public void clearSurplusData() {
        super.clearSurplusData();
        for (LocalAbstractObject object : objectsToDelete) {
            object.clearSurplusData();        
        }
        for (LocalAbstractObject object : objectsActuallyDeleted) {
            object.clearSurplusData();
        }
    }
}
