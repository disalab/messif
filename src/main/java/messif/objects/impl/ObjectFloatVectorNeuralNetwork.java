package messif.objects.impl;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.BitSet;
import messif.objects.LocalAbstractObject;
import messif.objects.ObjectVector;
import messif.objects.impl.ObjectFloatVector;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializable;
import messif.objects.nio.BinarySerializator;


/**
 * This object uses static array of floats as its data content. The data is serialized in a special way fitting 
 *  the neural network output (Caffe).
 * No implementation of distance function is provided.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class ObjectFloatVectorNeuralNetwork extends LocalAbstractObject implements BinarySerializable, ObjectVector<Float> {
    
    /** class id for serialization */
    private static final long serialVersionUID = 450301L;
    
    /**  A bit array saying which coordinate is zero and which is not  */
    protected transient WeakReference<BitSet> zeroes = null;

    /**  A bit array saying which coordinate is zero and which is not  */
    protected transient WeakReference<float []> compressedData = null;
    
    /** Data array (only the non-zero dimensions) */
    protected final float[] data;


    //****************** Constructors ******************//

    /**
     * Creates a new instance of ObjectFloatVector.
     * @param data the data content of the new object
     */
    public ObjectFloatVectorNeuralNetwork(float[] data) {
        this.data = data;
    }

    /**
     * Creates a new instance of ObjectFloatVector.
     * @param locatorURI the locator URI for the new object
     * @param data the data content of the new object
     */
    public ObjectFloatVectorNeuralNetwork(String locatorURI, float[] data) {
        super(locatorURI);
        this.data = data;
    }

    /**
     * Creates a new instance of ObjectFloatVector from text stream.
     * @param stream the stream from which to read lines of text
     * @throws EOFException if the end-of-file of the given stream is reached
     * @throws IOException if there was an I/O error during reading from the stream
     * @throws NumberFormatException if a line read from the stream does not consist of comma-separated or space-separated numbers
     */
    public ObjectFloatVectorNeuralNetwork(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
        // Keep reading the lines while they are comments, then read the first line of the object
        String line = readObjectComments(stream);
        this.data = ObjectFloatVector.parseFloatVector(line);
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        ObjectFloatVector.writeFloatVector(data, stream, ',', '\n');
    }
        
    /**
     * Create the compressed data + zeroes bit set from a full float array.
     */
    protected float [] getCompressedData(BitSet zeroesToInit) throws IllegalArgumentException {
        SimpleFloatArray retVal = new SimpleFloatArray(data.length / 2);
        for (int i = 0; i < data.length; i++) {
            if (data[i] == 0f) {
                zeroesToInit.set(i);
            } else {
                retVal.add(data[i]);
            }
        }
        return retVal.toArray();
    }
    
    /** Simple encapsulation of a variable-size array of floats. Only the essential methods implemented. */
    private static class SimpleFloatArray {
        private static final float INCREASE_FACTOR = 1.3f;
        
        /** Actual float data. */
        float [] data;
        
        /** Current size of the array. */
        int size = 0;

        public SimpleFloatArray(int initialSize) {
            data = new float [initialSize];
        }
        
        public void add(float value) {
            // increase the storage size
            if (data.length <= size) {
                float [] newData = new float [(int) (((float) data.length) * INCREASE_FACTOR)];
                System.arraycopy(data, 0, newData, 0, data.length);
                data = newData;
            }
            data[size ++] = value;
        }
        
        public float [] toArray() {
            return Arrays.copyOf(data, size);
        }
    }

    /**
     * Create the uncompressed data given the compressed data.
     * @return 
     */
    protected static float [] getRawData(int length, BitSet zeroes, float [] compressedData) {
        float [] retVal = new float [length];
        int dataCounter = 0;
        for (int i = 0; i < length; i++) {
            if (! zeroes.get(i)) {
                retVal[i] = compressedData[dataCounter ++];
            }
        }
        return retVal;
    }
    
    public float[] getRawData() {
        return Arrays.copyOf(this.data, this.data.length);
    }

    @Override
    public Float getVectorDataItem(int index) {
        return this.data[index];
    }

    @Override
    public int getVectorDataDimension() {
        return this.data.length;
    }

    @Override
    public <S> S[] getVectorData(S[] template) {
        if (template != null && this.data != null && template.getClass().isAssignableFrom(this.data.getClass()))
            return (S[])(Object)(this.data.clone());
        return null;
    }

    // ********************      Overrides    ********************************** //
    
    @Override
    public boolean dataEquals(Object obj) {
        if (!(obj instanceof ObjectFloatVectorNeuralNetwork))
            return false;

        return Arrays.equals(((ObjectFloatVectorNeuralNetwork)obj).data, data);
    }

    @Override
    public int getSize() {
        return this.data.length * Float.SIZE / 8;
    }
    
    
    @Override
    public int dataHashCode() {
        return Arrays.hashCode(data);
    }
    
    
    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of ObjectFloatVector loaded from binary input buffer.
     *
     * @param input the buffer to read the ObjectFloatVector from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the buffer
     */
    protected ObjectFloatVectorNeuralNetwork(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
        int length = serializator.readInt(input);
        BitSet zeroesLocal = BitSet.valueOf(serializator.readLongArray(input));
        data = getRawData(length, zeroesLocal, serializator.readFloatArray(input));
    }

    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        BitSet zeroesLocal = (zeroes == null) ? null : zeroes.get();
        float [] compressedDataLocal = (compressedData == null) ? null : compressedData.get();
        if (zeroesLocal == null || compressedDataLocal == null) {
            zeroesLocal = new BitSet(data.length);
            compressedDataLocal = getCompressedData(zeroesLocal);
            zeroes = new WeakReference<>(zeroesLocal);
            compressedData = new WeakReference<>(compressedDataLocal);
        }
        return super.binarySerialize(output, serializator) + serializator.write(output, data.length)
                + serializator.write(output, zeroesLocal.toLongArray()) + serializator.write(output, compressedDataLocal);
    }

    @Override
    public int getBinarySize(BinarySerializator serializator) {
        BitSet zeroesLocal = (zeroes == null) ? null : zeroes.get();
        float [] compressedDataLocal = (compressedData == null) ? null : compressedData.get();
        if (zeroesLocal == null || compressedDataLocal == null) {
            zeroesLocal = new BitSet(data.length);
            compressedDataLocal = getCompressedData(zeroesLocal);
            zeroes = new WeakReference<>(zeroesLocal);
            compressedData = new WeakReference<>(compressedDataLocal);
        }
        return  super.getBinarySize(serializator) + serializator.getBinarySize(data.length)
                + serializator.getBinarySize(zeroesLocal.toLongArray()) + serializator.getBinarySize(compressedDataLocal);
    }

}
