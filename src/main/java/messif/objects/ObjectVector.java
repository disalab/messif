/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.objects;

/**
 * "Marker" class of all vector-based objects
 * T is the type of the vector item.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <T> T is any numeric type
 */
public interface ObjectVector<T extends Number> {

    public abstract T getVectorDataItem(int index);

    /** 
     * Return an array of (typically) primitive type values.
     * @param <S> primitive typa that must match T
     * @param template just for inferring the correct return type
     * @return internal vector of data
     */
    public abstract <S> S[] getVectorData(S[] template);
    
    public int getVectorDataDimension();
}
