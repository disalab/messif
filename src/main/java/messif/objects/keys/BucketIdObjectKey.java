/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.objects.keys;

import java.io.IOException;
import java.io.Serializable;
import messif.objects.keys.AbstractObjectKey;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializator;

/**
 *
 * @author xdohnal
 */
public class BucketIdObjectKey extends AbstractObjectKey implements Serializable {
    /** Class serial id for serialization. */
    private static final long serialVersionUID = 1L;

    private int bucketId;  
    
    /** usefulness counter for query-driven optimization*/
    private int objectUsefulness;
    
    public BucketIdObjectKey(String locatorURI, int id) {
        super(locatorURI);
        bucketId = id;
    }

    public int getBucketId() {
        return bucketId;
    }
    
    public void incUsefulness() {
        objectUsefulness ++;
    }
    
    public int getUsefulness() {
        return objectUsefulness;
    }
    
    @Override
    public String toString() {
        return super.toString() + ":" + bucketId;
    }

    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + serializator.getBinarySize(bucketId);
    }

    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) + serializator.write(output, bucketId);
    }

    @Override
    public BucketIdObjectKey clone(String locatorURI) throws CloneNotSupportedException {
        BucketIdObjectKey key = (BucketIdObjectKey) super.clone(locatorURI);
        key.bucketId = bucketId;
        return key;
    }
        
    /** Replace this class with the new version of it */
    public Object readResolve() {
        return new messif.objects.keys.BucketIdObjectKey(this.getLocatorURI(), this.getBucketId());
    }
    
}
