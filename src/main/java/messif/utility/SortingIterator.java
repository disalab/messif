/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Iterator that retrieves the smallest object from a collection of iterators when
 * its {@link #next() next} method is called.
 * Note that iterators can be added after the iterator is constructed until the {@link #next()} method
 * is called for the first time. Then the iterator is locked and no additional data can be added.
 *
 *
 * @param <T> type of objects provided by the encapsulated collection of iterators
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SortingIterator<T> implements PeekIterator<T>, Comparator<T> {
    /** The collection of encapsulated iterators */
    private final List<PeekItem> iterators;
    /** Flag whether this iterator has finished initializing the iterators and has started to iterate over the values */
    private boolean initialized;
    /** The object returned by the last {@link #next()} method call */
    private T lastObject;
    /** The comparator used to sort the provided objects */
    private final Comparator<? super T> comparator;

    /**
     * Internal class that encapsulates iterator, holds the last element got from
     * {@link #next()} and makes it comparable using the comparator of the 
     * encapsulating class on the current items.
     */
    private class PeekItem extends PeekIteratorImpl<T> implements Comparable<PeekItem> {
        /**
         * Creates a new instance of PeekItem encapsulating a given iterator.
         * @param iterator the iterator to encapsulate
         */
        private PeekItem(Iterator<? extends T> iterator) {
            super(iterator);
        }
        /**
         * Creates a new instance of PeekItem encapsulating a given array.
         * @param <V> type of the values in the array
         * @param array the array to encapsulate
         */
        private <V extends T> PeekItem(V[] array) {
            super(array);
        }
        @Override
        public int compareTo(PeekItem item) {
            int ret = compare(peek(), item.peek());
            if (ret == 0)
                return hashCode() - item.hashCode();
            else
                return ret;
        }
    }

    /**
     * Creates a new empty instance of SortingIterator using the natural comparator of the object.
     * Note that iterators must be added before the {@link #next()} method is called.
     */
    public SortingIterator() {
        this(null);
    }

    /**
     * Creates a new empty instance of SortingIterator.
     * Note that iterators must be added before the {@link #next()} method is called.
     * @param comparator the comparator used to sort the objects provided by iterators
     */
    public SortingIterator(Comparator<? super T> comparator) {
        this.comparator = comparator;
        this.iterators = new ArrayList<PeekItem>();
    }

    /**
     * Creates a new instance of SortingIterator.
     * @param iterators the collection of iterators to get the objects from
     * @param comparator the comparator used to sort the objects provided by iterators
     */
    public SortingIterator(Collection<? extends Iterator<? extends T>> iterators, Comparator<? super T> comparator) {
        this.comparator = comparator;
        this.iterators = new ArrayList<PeekItem>(iterators.size());
        for (Iterator<? extends T> iterator : iterators)
            addIterator(iterator);
    }

    /**
     * Initialize the list of stored value providers.
     * Note that after this method is called, no additional value providers can be added.
     */
    protected void initialize() {
        Collections.sort(this.iterators);
        initialized = true;
    }

    /**
     * Adds additional iterator to this sorting iterator.
     * Note that the iterator is expected to provide values sorted according to this iterator's comparator.
     * @param iterator the iterator providing the values
     * @return this instance to allow chaining
     * @throws IllegalStateException if the {@link #next()} method was already called
     */
    public final SortingIterator<T> addIterator(Iterator<? extends T> iterator) throws IllegalStateException {
        if (initialized)
            throw new IllegalStateException("Cannot add iterator after the next method was called");
        if (iterator != null && iterator.hasNext())
            this.iterators.add(new PeekItem(iterator));
        return this;
    }

    /**
     * Adds additional array to this sorting iterator.
     * Note that the array is expected to have values sorted according to this iterator's comparator.
     * @param <V> class of the values in the array
     * @param array the array with the sorted values
     * @return this instance to allow chaining
     * @throws IllegalStateException if the {@link #next()} method was already called
     */
    public final <V extends T> SortingIterator<T> addArray(final V[] array) {
        if (initialized)
            throw new IllegalStateException("Cannot add array after the next method was called");
        if (array != null && array.length > 0)
            this.iterators.add(new PeekItem(array));
        return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public final int compare(T o1, T o2) {
        if (o1 == null)
            return o2 == null ? 0 : 1;
        if (o2 == null)
            return -1;
        if (comparator == null)
            return ((Comparable<? super T>)o1).compareTo(o2);
        return comparator.compare(o1, o2);
    }

    @Override
    public boolean hasNext() {
        return !iterators.isEmpty();
    }

    @Override
    public T next() {
        if (!initialized)
            initialize();

        // Next returns null if there are no additional data
        if (iterators.isEmpty())
            return null;

        // Get the first iterator (which has the smallest current object)
        PeekItem iterator = iterators.get(0);

        // Retrieve the current object from the iterator
        lastObject = iterator.next();

        // Resort the iterators according to the current value
        if (iterator.hasNext()) {
            int pos = -(Collections.binarySearch(this.iterators.subList(1, this.iterators.size()), iterator) + 1);
            if (pos > 0) {
                iterators.remove(0);
                iterators.add(pos, iterator);
            }
        } else {
            iterators.remove(0);
        }

        return lastObject;
    }

    @Override
    public T peek() throws NoSuchElementException {
        if (!hasNext())
            return null;
        return iterators.get(0).peek();
    }

    /**
     * Returns the object returned by the last {@link #next()} method call.
     * @return the object returned by the last {@link #next()} method call
     */
    public T last() {
        return lastObject;
    }

    /**
     * Returns <tt>true</tt> (and advances this iterator) if the next value in this iterator
     * is (comparator) equal to the given {@code value}.
     * @param value the value to check
     * @return <tt>true</tt> if this iterator was advanced to the next value or <tt>false</tt> otherwise.
     */
    public boolean nextIfEqual(T value) {
        if (!hasNext())
            return false;
        if (compare(peek(), value) != 0)
            return false;
        next();
        return true;
    }

    /**
     * Read all items (by calling {@link #next()}) that have the same value
     * as the current item.
     * @return the number of duplicates skipped
     * @throws IllegalStateException if the iterator has no current value
     */
    public int skipDuplicates() throws IllegalStateException {
        int count = 0;
        while (nextIfEqual(lastObject))
            count++;
        return count;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }


    /////////////////////////////// Intersection ///////////////////////////////

    /**
     * Represents resulting values that can be returned by
     * {@link IntersectingIterator#intersect(IntersectingIterator) intersect}
     * method of the {@link IntersectingIterator}.
     */
    public static enum IntersectionResult {
        /**
         * There were no intersecting objects found. Both iterators are completely read (i.e. have no next objects).
         */
        NONE,
        /** A new intersecting object was found in both iterators */
        BOTH,
        /**
         * A new intersecting object was found in the iterator, on which the intersect
         * method was called. The iterator passed as the argument did not advance.
         */
        THIS_ONLY,
        /**
         * A new intersecting object was found in the iterator, which was passed
         * as the argument of the intersect method. The iterator on which the intersect
         * was called did not advance.
         */
        ARGUMENT_ONLY;

        /**
         * Returns <tt>true</tt> if the result indicates that there was an intersecting object found.
         * Otherwise, <tt>false</tt> is returned, i.e. the results is {@link #NONE}.
         * @return <tt>true</tt> if the result indicates that there was an intersecting object found
         */
        public boolean isIntersecting() {
            return this != NONE;
        }

        /**
         * Returns <tt>true</tt> if the result indicates that there was an
         * intersecting object found in the iterator on which the intersect method was called.
         * @return <tt>true</tt> if the result indicates that there was an
         *      intersecting object found in the iterator on which the intersect method was called
         */
        public boolean isThisIntersecting() {
            return this == BOTH || this == THIS_ONLY;
        }

        /**
         * Returns <tt>true</tt> if the result indicates that there was an
         * intersecting object found in the iterator passed as argument.
         * @return <tt>true</tt> if the result indicates that there was an
         *      intersecting object found in the iterator passed as argument
         */
        public boolean isArgumentIntersecting() {
            return this == BOTH || this == ARGUMENT_ONLY;
        }

        /**
         * Returns <tt>true</tt> if the result indicates that there was an intersecting
         * object found both this iterator and the iterator passed as argument.
         * @return <tt>true</tt> if the result indicates that there was an intersecting
         *      object found both this iterator and the iterator passed as argument
         */
        public boolean isBothIntersecting() {
            return this == BOTH;
        }
    };

    /**
     * Find the next value that is present in this iterator and the given {@code iterator}.
     * The return value indicates whether {@link IntersectionResult#NONE no intersecting}
     * items were found (and either this or the given {@code iterator} has no more items),
     * or an intersecting item was found. In the later case the current item of
     * both iterators is the same. Note that there can be duplicate items in which
     * case the next call to this method will return either {@link IntersectionResult#THIS_ONLY}
     * or {@link IntersectionResult#ARGUMENT_ONLY} value to indicate
     * that a duplicate in the respective iterator was found.
     *
     * @param iterator the iterator to intersect with
     * @return {@link IntersectionResult#NONE} if one of the iterators reached last item without a matching value,
     *         or one of the other three {@link IntersectionResult} values if a value in both the iterators was found
     *         (their current value will be the same)
     */
    public IntersectionResult intersect(SortingIterator<T> iterator) {
        // If this iterator has the same next value as the other iterator's current value (duplicate objects)
        if (nextIfEqual(iterator.last()))
            return IntersectionResult.THIS_ONLY;

        // If the other iterator has the same next value as this iterator's current value (duplicate objects)
        if (iterator.nextIfEqual(this.last()))
            return IntersectionResult.ARGUMENT_ONLY;

        // If there are no items in either iterator, exit
        if (!hasNext() || !iterator.hasNext())
            return IntersectionResult.NONE;

        // Read next item from both iterators - it is either at the beggining or after an intersection is found
        T thisVal = next();
        T otherVal = iterator.next();

        // Repeat until an intersection is found or one of the iterators runs out of items
        for (;;) {
            int cmp = compare(thisVal, otherVal);
            if (cmp < 0) { // This iterator's value is smaller, advance this iterator or exit
                if (!hasNext())
                    return IntersectionResult.NONE;
                thisVal = next();
            } else if (cmp > 0) { // Other iterator's value is smaller, advance the other iterator or exit
                if (!iterator.hasNext())
                    return IntersectionResult.NONE;
                otherVal = iterator.next();
            } else {
                return IntersectionResult.BOTH;
            }
        }
    }

    /**
     * Find the next value that is present in this iterator and the given {@code iterator}.
     * The return value indicates whether {@link IntersectionResult#NONE no intersecting}
     * items were found (and either this or the given {@code iterator} has no more items),
     * or an intersecting item was found. In the later case the current item of
     * both iterators is the same. Note that there can be duplicate items in which
     * case the next call to this method will return either {@link IntersectionResult#THIS_ONLY}
     * or {@link IntersectionResult#ARGUMENT_ONLY} value to indicate
     * that a duplicate in the respective iterator was found. This can be avoided
     * if {@code duplicates} array is passed in which case the array is filled with
     * the number of duplicate items in this and the given {@code iterator}.
     *
     * @param iterator the iterator to intersect with
     * @param frequency if an array instance is provided, the first item will be set
     *          to the number of duplicate items encountered (skipped) in this iterator
     *          when an intersection is found and the second array item is set to the
     *          number of duplicates in the {@code iterator};
     *          this is ignored if <tt>null</tt> is passed
     * @return <tt>true</tt> if a value in both the iterators was found (their current value will be the same)
     *      or <tt>false</tt> if one of the iterators reached last item without a matching value
     */
    public IntersectionResult intersectSkipDuplicates(SortingIterator<T> iterator, int[] frequency) {
        IntersectionResult result = intersect(iterator);
        // Skip all duplicates and set the frequencies into the array
        if (result != IntersectionResult.NONE && frequency != null && frequency.length == 2) {
            frequency[0] = 1 + skipDuplicates();
            frequency[1] = 1 + iterator.skipDuplicates();
        }
        return result;
    }
}
